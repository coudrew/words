import { test, expect, describe } from 'vitest';
import { render, screen, waitfor, act } from '@testing-library/react';
import SheetContext from '../app/components/SheetContext.jsx';
import NotifyModal from '../app/components/NotifyModal.jsx';

describe('NotifyModal', () => {

    test('shows a loading message', () => {
        render(<SheetContext modalDefault='loading'><NotifyModal /></SheetContext>);
        const modalText = screen.getByTestId('notifyText');
        expect(modalText.textContent).toEqual('loading...');
    });
    test('shows an error message and button', () => {
        render(<SheetContext modalDefault='error'><NotifyModal /></SheetContext>);
        const modal = screen.getByTestId('notifyText');
        const okButton = screen.getByTestId('modalOk');
        expect(okButton).toBeInTheDocument();
        expect(modal.textContent).toEqual('Something went wrong');
    });
});
