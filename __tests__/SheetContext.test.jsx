import { test, expect, describe } from 'vitest';
import { render, screen, waitFor, act } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import SheetContext, { SongContext } from '../app/components/SheetContext.jsx';
import NotifyModal from '../app/components/NotifyModal.jsx';
import { useContext } from 'react';

function TestConsumer() {
    const value = useContext(SongContext);
    const { passages,
        title,
        selectedPassage,
        updateTitle,
        selectPassage,
        addPassage,
        updatePassage,
        removePassage } = value;
    
    return selectedPassage ? (<>
        <button data-testid="title_button" onClick={updateTitle} value={'song'}>setTitle</button>
        {passages.map((passage, i) => <button key={`passageButton${i}`} data-testid={`passage-${i}`} id={passage.id} onClick={selectPassage}>{`${passage.id}`}</button>)}
        <button data-testid="addPassage" onClick={() => addPassage()}>add Passage</button>
        <button data-testid="updateLyrics" onClick={updatePassage} id={selectedPassage.id} name='lyrics' value={"updated lyrics"}>update lyircs</button>
        <button data-testid="updateChords" onClick={updatePassage} id={selectedPassage.id} name='chords' value={"D5"}>update chords</button>
        <button data-testid="updateType" onClick={updatePassage} id={selectedPassage.id} name='passageType' value={"chorus"}>update type</button>
        <button data-testid="removePassage" onClick={removePassage} id={selectedPassage.id}>remove Passage</button>
        <span data-testid="title">{title}</span>
        <span data-testid="lyrics">{selectedPassage.lyrics}</span>
        <span data-testid="chords">{selectedPassage.chords}</span>
        <span data-testid="type">{selectedPassage.passageType}</span>
        <span data-testid="id">{selectedPassage.id}</span>
    </>) : null
}

describe('SheetContext', () => {

    test('creates a default verse passage', async () => {
        render(<SheetContext><TestConsumer /></SheetContext>);
        await waitFor(() => {  
            const lyircs = screen.getByTestId('lyrics');
            const chords = screen.getByTestId('chords');
            const type = screen.getByTestId('type');
            const id = screen.getByTestId('id');
            expect(lyircs).toHaveTextContent('');
            expect(chords).toHaveTextContent('');
            expect(type).toHaveTextContent('verse');
        })
    });
    test('adds a new passage', async () => {
        render(<SheetContext><TestConsumer /></SheetContext>);
        await act(() => userEvent.click(screen.getByTestId('addPassage')));
        await waitFor(() => {
            const id = screen.getByTestId('id').textContent;
            const passage = screen.getByTestId('passage-1');
            expect(id).toEqual(passage.textContent);
        });
    });
    test('updates title', async () => {
        render(<SheetContext><TestConsumer /></SheetContext>);
        await act(() => userEvent.click(screen.getByTestId('title_button')));
        const title = screen.getByTestId('title');
        await waitFor(() => expect(title.textContent).toEqual('song'));
    });
    test('updates passage', async () => {
        render(<SheetContext><TestConsumer /></SheetContext>);
        await act(() => {
            userEvent.click(screen.getByTestId('updateLyrics'));
        });
        await waitFor(() => {
            const lyrics = screen.getByTestId('lyrics');
            expect(lyrics.textContent).toEqual('updated lyrics');
        });
        await act(() => {
            userEvent.click(screen.getByTestId('updateChords'));
        });
        await waitFor(() => {
            const chords = screen.getByTestId('chords');
            expect(chords.textContent).toEqual('D5');
        });
        await act(() => {
            userEvent.click(screen.getByTestId('updateType'));
        });
        await waitFor(() => {
            const type = screen.getByTestId('type');
            expect(type.textContent).toEqual('chorus');
        });
    });
    test('removes a passage', async () => {
        render(<SheetContext><TestConsumer /></SheetContext>);
        await act(() => userEvent.click(screen.getByTestId('addPassage')));
        await waitFor(() => {
            const passages = screen.getAllByTestId(/passage-/);
            expect(passages.length).toEqual(2);
        })
        await act(() => userEvent.click(screen.getByTestId('removePassage')));
        await waitFor(() => {
            const passages = screen.getAllByTestId(/passage-/);
            expect(passages.length).toEqual(1)
        });
    });
    test('controls a modal', async () => {
        render(<SheetContext modalDefault='error'><NotifyModal /><TestConsumer /></SheetContext>);
        const modal = screen.getByTestId('notifyModal');
        expect(modal).toBeInTheDocument();
        await act(() => userEvent.click(screen.getByTestId('modalOk')));
        await waitFor(() => {
            const possibleModal = screen.queryByTestId('notifyModal');
            expect(possibleModal).toBeNull();
        })
    });
});