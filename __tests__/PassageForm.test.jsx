import { test, expect, describe } from 'vitest';
import { render, screen, act, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import PassageForm from '../app/components/PassageForm.jsx';
import SheetContext from '../app/components/SheetContext.jsx';

describe('PassageForm', () => {

    test('has a text input to edit title', async () => {
        render(<SheetContext><PassageForm /></SheetContext>);
        await act(() => userEvent.type(screen.getByTestId('titleInput'), ' song'));
        await waitFor(() => {
            const input = screen.getByTestId('titleInput');
            expect(input).toHaveValue('Untitled song');
        })
    });
    test('has a select input to select passage type', async () => {
        render(<SheetContext><PassageForm /></SheetContext>);
        userEvent.selectOptions(screen.getByRole('combobox'), screen.getByRole('option', {name: 'intro'}));
        await waitFor(() => {
            expect(screen.getByRole('option', { name: 'intro' }).selected).toBe(true);
            expect(screen.getByTestId('passageTypeSelect')).toHaveValue('intro');
        })
    });
    test('has a textarea input to edit chords', async () => {
        render(<SheetContext><PassageForm /></SheetContext>);
        await act(() => userEvent.type(screen.getByTestId('chordsInput'), 'D'));
        await waitFor(() => {
            const input = screen.getByTestId('chordsInput');
            expect(input).toHaveValue('D');
        })
    });
    test('has a textarea iput to edit lyrics', async () => {
        render(<SheetContext><PassageForm /></SheetContext>);
        await act(() => userEvent.type(screen.getByTestId('lyricsInput'), 'w'));
        await waitFor(() => {
            const input = screen.getByTestId('lyricsInput');
            expect(input).toHaveValue('w');
        })
    });
});