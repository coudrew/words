import { test, expect, describe, vi } from 'vitest';
import { render, screen, act, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import SheetContext from '../app/components/SheetContext.jsx';
import PassageList, {PassageListItem} from '../app/components/PassageList.jsx';


describe('PassageList', () => {

    const passage = {
        id: 'testPassage',
        passageType: 'verse'
    };
    test('has correct styles and displays passageType', () => {
        render(<PassageListItem testid={'passage-0'} passage={passage} selected={true} />);
        const passageListItem = screen.getByTestId('passage-0');
        expect(passageListItem).toHaveClass('button list-item--active');
        expect(passageListItem.textContent).toEqual('verse');
    });
    test('calls selectPassage on click', async () => {
        const selectPassage = vi.fn();
        render(<PassageListItem testid={'passage-0'} passage={passage} selectPassage={selectPassage} />)
        await act(() => userEvent.click(screen.getByTestId('passage-0')));
        expect(selectPassage).toHaveBeenCalledTimes(1);
    })

    test('displays a list of passages', () => {
        render(<SheetContext><PassageList /></SheetContext>);
        const passages = screen.getAllByTestId(/passage-/);
        expect(passages.length).toEqual(1);
    });
    test('has controls to add or remove a passage', async () => {
        render(<SheetContext><PassageList /></SheetContext>);
        await act(() => userEvent.click(screen.getByTestId('addPassage')));
        const passages = screen.getAllByTestId(/passage-/);
        await waitFor(() => {
            expect(passages.length).toEqual(2)
        })
        await act(() => userEvent.click(screen.getByTestId('removePassage')));
        const updatedPassages = screen.getAllByTestId(/passage-/);
        await waitFor(() => {
            expect(updatedPassages.length).toEqual(1)
        });
    });
    test('has a control to copy a passage', async () => {
        render(<SheetContext><PassageList /></SheetContext>)
        await act(() => userEvent.click(screen.getByTestId('copyPassage')));
        const copiedPassages = screen.getAllByTestId(/passage-/);
        await waitFor(() => {
            expect(copiedPassages.length).toEqual(2);
        });
    })

});