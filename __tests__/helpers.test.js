import { test, expect } from "vitest";
import { preparePassage, prepareSong, extractChords, extractNonSubBeatChords, capitalizeNoteName } from "../helpers";
import { verseChords, verseLyrics1, verseLyrics2, bridgeChords, bridgeLyrics, chorusChords, chorusLyrics } from "./testData";

test('extractNonSubBeatChords returns an array of chords', () => {
    const chordString = 'D5 B5 C#  Em G/D';
    const expected = ['D5', 'B5', 'C#', 'Em', 'G/D'];
    const actual = extractNonSubBeatChords(chordString);
    expect(actual).toEqual(expected);
});

test('extractChords returns an array of chords', () => {
    const chordString = 'D5 B5 C# Em';
    const expected = ['D5', 'B5', 'C#', 'Em'];
    const actual = extractChords(chordString);
    expect(actual).toEqual(expected);
});

test('extractChords removes beat notation', () => {
    const chordString = 'D5.. B5.. C#. Em...';
    const expected = ['D5', 'B5', 'C#', 'Em'];
    const actual = extractChords(chordString);
    expect(actual).toEqual(expected);
});

test('extractChords handles sub-beat notation', () => {
    const chordString = 'D5 B5 [C Em]';
    const expected = ['D5', 'B5', '[C Em]'];
    const actual = extractChords(chordString);
    expect(actual).toEqual(expected);
});

test('capitalizeNoteName captializes note name', () => {
    const chordString = 'd5 B5 [d em]';
    const expected = 'D5 B5 [D Em]';
    const actual = capitalizeNoteName(chordString);
    expect(actual).toEqual(expected);
});

test('preparePassage prepares a passage chord and lyric lines for parser', () => {
    const expected = {
        passageString: "#v\nD5 B5\n_Rebel catchphrases and _malcontented hair\nD5 B5\n_RevolutionTM _slogans everywhere\nA5 G5\n_Identity in crisis or _ego in repair\nB5 A5\n_sick of all their friends and other _creatures of the were\n",
        uniqueChords: ['D5', 'B5', 'A5', 'G5']
    }
    const actual = preparePassage({ lyrics: verseLyrics1, chords: verseChords, passageType: 'verse' });
    expect(actual.passageString).toEqual(expected.passageString);
    expect(actual.uniqueChords).toEqual(expected.uniqueChords);
});

test('preparePassage handles beat indicators from chords', () => {
    const expected = {
        passageString: "#c\nF#5. G5...\n_Hold to that _teenage motto\nF#5. G5...\n_and maybe _then tomorrow\nE5. G5...\n_they'll lay their _heads down on the\nD5....\n_greener grass of Toronto\n",
        uniqueChords: ['F#5', 'G5', 'E5', 'D5']
    };
    const actual = preparePassage({ lyrics: chorusLyrics, chords: chorusChords, passageType: 'chorus' });
    expect(actual.passageString).toEqual(expected.passageString);
    expect(actual.uniqueChords).toEqual(expected.uniqueChords);
});

test('prepareSong combines passages and unique chords', () => {
    const expected = {
        song: "#v\nD5 B5\n_Rebel catchphrases and _malcontented hair\nD5 B5\n_RevolutionTM _slogans everywhere\nA5 G5\n_Identity in crisis or _ego in repair\nB5 A5\n_sick of all their friends and other _creatures of the were\n#c\nF#5. G5...\n_Hold to that _teenage motto\nF#5. G5...\n_and maybe _then tomorrow\nE5. G5...\n_they'll lay their _heads down on the\nD5....\n_greener grass of Toronto\n",
        chords: ['D5', 'B5', 'A5', 'G5', 'F#5', 'E5']
    };
    const verse = preparePassage({ lyrics: verseLyrics1, chords: verseChords, passageType: 'verse' });
    const chorus = preparePassage({ lyrics: chorusLyrics, chords: chorusChords, passageType: 'chorus' });
    const actual = prepareSong([verse, chorus]);
    expect(actual.song).toEqual(expected.song);
    expect(actual.chords).toEqual(expected.chords);
});