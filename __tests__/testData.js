import { test } from "vitest";
export const verseLyrics1 = "_Rebel catchphrases and _malcontented hair \n_RevolutionTM _slogans everywhere\n_Identity in crisis or _ego in repair\n_sick of all their friends and other _creatures of the were";
export const chorusLyrics = "_Hold to that _teenage motto\n_and maybe _then tomorrow\n_they'll lay their _heads down on the\n_greener grass of Toronto";
export const verseLyrics2 = "_Deep in your heart I _think you knew\n_You'd never escape that first _person view\nYou've made your bed and now there's _nothing left to do\n_but lie awake like day-glo _orange on North Atlantic blue";
export const bridgeLyrics = "_No job _yet, it's _getting colder\n_Pan up _slow from a _saccharine moment\n_Scene ends and _fades to black\n_New act _cue soundtrack\n_a weepy melo_dy enters, they\n_swear they're never going back";
export const verseChords = "D5 B5\nD5 B5\nA5 G5\nB5 A5";
export const chorusChords = "F#5. G5...\nF#5. G5...\nE5. G5...\nD5....";
export const bridgeChords = "B5 A5 G5 G5\nB5 A5 G5 G5\nF#5. G5...\nF#5. G5...\nE5. G5...\nD5....";

test('skips', () => { });