import { test, expect, describe, beforeEach, vi } from 'vitest';
import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Select from '../app/components/Select.jsx';

describe('Select', () => {
    const items = ['a', 'b', 'c', 'd'];
    let onSelect;
    beforeEach(() => {
        onSelect = vi.fn();
        render(<Select value={items[0]} options={items} onSelect={onSelect} name="Select" />);
    });

    test('it renders a list of strings passed to it as options', () => {
        const options = screen.getAllByRole('option');
        expect(options.length).toEqual(4);
        items.forEach((item, i) => {
            expect(options[i]).toHaveValue(item);
        });
    });

    test('it takes a value prop and sets that option to selected', () => {
        const select = screen.getByRole('combobox');
        const options = screen.getAllByRole('option');
        expect(select).toHaveValue(items[0]);
        expect(options[0].selected).toBe(true);
    });

    test('it passes name down to underlying select', () => {
        const select = screen.getByRole('combobox');
        expect(select.name).toEqual('Select');
    });

    test('it passes onSelect to underlying select onChange', () => {
        userEvent.selectOptions(screen.getByRole('combobox'), 'b');
        waitFor(() => {
            const options = screen.getAllByRole('option');
            expect(options[1].selected).toBe(true);
            expect(onSelect).toHaveBeenCalled();
        })
    });
});