function InfoArea() {
    return (
        <div className="infoArea">
            <h2>Words</h2>
            <span>chord chart editor</span>
            <h4>by Andrew Couture</h4>
            <p>
                Built with <a href="https://nextjs.org">Next.JS</a> and <a href="https://npmjs.com/package/chord-mark">chord-mark</a>.
                For a full explanation of ChordMark syntax, see <a href="https://chordmark.netlify.app/docs/reference/chords">here</a>
            </p>
            <ul>
                <li>add chords, ie: B5, F#m, in the Chords text area</li>
                <li>add a new line of chords for each measure (assuming a 4 beat measure currently)</li>
                <li>add the corresponding lyrics in the Lyrics text area, mark the placement of a chord with an underscore</li>
                <li>add a new line of lyrics for each measure</li>
            </ul>
            <h3>Coming Soon!</h3>
            <p>
                Sub-beat notation, chord diagrams
            </p>
        </div>
    )
}

export default InfoArea;