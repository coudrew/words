'use client'
import { useContext, useEffect, useRef } from "react";
import { SongContext } from "./SheetContext";
import Select from "./Select";

function PassageForm() {
    const chordsRef = useRef();
    const lyricsRef = useRef();
    const textRefMap = {
        chords: chordsRef,
        lyrics: lyricsRef
    };
    const { selectedPassage = {}, updatePassage, updateTitle, title, updateArtist, artist } = useContext(SongContext);
    const { lyrics, chords, passageType, cursor = {} } = selectedPassage;
    useEffect(() => {
        const { selectionStart, name } = cursor;
        if (selectionStart) {
            textRefMap[name].current.selectionStart = selectionStart;
            textRefMap[name].current.selectionEnd = selectionStart;
        }
    }, [cursor])
    return (<div>
        <form className="form">
            <label className="form__label" htmlFor='title'>Song Title</label>
            <input type="text" name="title" id="titleInput" data-testid="titleInput" value={title} onChange={updateTitle} />
            <label className="form__label" htmlFor='title'>Artist</label>
            <input type="text" name="artist" id="artistInput" data-testid="artistInput" value={artist} onChange={updateArtist} />
            <label className="form__label" htmlFor='passageType'>Passage Type</label>
            <Select value={passageType} options={['intro', 'verse', 'pre-chorus', 'chorus', 'bridge', 'solo', 'interlude', 'adlib', 'outro']}
                onSelect={updatePassage}
                name="passageType"
                id={selectedPassage.id}
                data-testid="passageTypeSelect"
            />
        
            <label className="form__label" htmlFor="chords">Chords</label>
            <textarea ref={chordsRef} name="chords" id={selectedPassage.id} data-testid="chordsInput" value={chords} onChange={updatePassage} />
            <label className="form__label" htmlFor="lyrics">Lyrics</label>
            <textarea ref={lyricsRef} className="lyrics" name="lyrics" id={selectedPassage.id} data-testid="lyricsInput" value={lyrics} onChange={updatePassage} />
        </form>
    </div>)
};

export default PassageForm;