function Select({ value, options, onSelect, name, ...rest }) {
    
    return (<select value={value} name={name} onChange={onSelect} {...rest}>
        {options.map((opt, i) => <option key={`${name}-${i}`} selected={opt === value} value={opt}>{opt}</option>)}
    </select>)
}

export default Select;