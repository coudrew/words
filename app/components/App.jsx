'use client';
import { Tab, Tabs, TabPanel, TabList } from "react-tabs";
import { useMediaQuery } from "@uidotdev/usehooks";
import styles from "../page.module.css";
import PassageForm from "./PassageForm";
import PassageList from "./PassageList";
import SheetViewer from "./SheetViewer";
import InfoArea from "./InfoArea";

const AppInner = ({ isMobile }) => {
  if (isMobile) {
    return (
      <>
        <Tabs>
          <TabList>
            <Tab>View</Tab>
            <Tab>Arrange</Tab>
            <Tab>Edit</Tab>
          </TabList>
          <TabPanel>
            <SheetViewer />
          </TabPanel>
          <TabPanel>
            <PassageList />
          </TabPanel>
          <TabPanel>
            <PassageForm />
          </TabPanel>
        </Tabs>
      </>
    )
  }
  return (
    <>
      <SheetViewer />
      <div className={styles.editor}>
        <PassageList />
        <div className={styles.controls}>
          <PassageForm />
          <InfoArea />
        </div>
      </div>
    </>
  )
};

export default function App() {
    const matches = useMediaQuery('(max-width: 1136px)');

  return (
    <main className={styles.main}>
      <AppInner isMobile={matches} />
    </main>
  );
}