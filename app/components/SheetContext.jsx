'use client'
import { createContext, useEffect, useState } from "react";
import { useIdle } from "@uidotdev/usehooks";
import { preparePassage, prepareSong, Passage } from "../../helpers";

export const SongContext = createContext();

function SheetContext({ children, modalDefault = '' }) {
    const [passages, setPassages] = useState([]);
    const [currentPassage, setCurrentPassage] = useState(0);
    const [selectedPassage, setSelectedPassage] = useState(passages[currentPassage]);
    const [modalType, setModalType] = useState(modalDefault)
    const [title, setTitle] = useState('Untitled');
    const [artist, setArtist] = useState('Unknown');
    const [songsheet, setSongsheet] = useState({song: '', chords: ''});
    const idle = useIdle(200);
    
    function selectPassage(event) {
        const { id } = event.target;
        const index = passages.findIndex(passage => passage.id === id);
        setCurrentPassage(index);
    };

    function addPassage(passageToCopy) {
        const passage = new Passage(passageToCopy);
        setPassages([...passages, passage]);
        setCurrentPassage(passages.length);
    };

    function updatePassage(event) {
        const { target: { name, value, id, selectionStart } } = event;
        setPassages(passages.map((passage) => passage.id === id ? preparePassage({ ...passage, [name]: value, cursor: {selectionStart, name} }) : passage));
    };

    function removePassage(event) {
        const { id: passageId } = selectedPassage;
        const nextPassages = passages.filter(({ id }) => id !== passageId);
        setPassages(nextPassages);
        if (currentPassage == nextPassages.length) {
            setCurrentPassage(nextPassages.length - 1);
        }
    };

    function updateTitle(event) {
        const { value } = event.target;
        setTitle(value);
    };

    function clearModal() {
        setModalType('');
    }

    function updateArtist(event) {
        const { value } = event.target;
        setArtist(value);
    }

    function prepareSongSheet() {
        const preparedSong = prepareSong(passages);
        setSongsheet(preparedSong);
    };

    useEffect(() => {
        if (!passages.length) {
            addPassage(new Passage());
        }
        if (selectedPassage !== passages[currentPassage]) {
            setSelectedPassage(passages[currentPassage])
        };
        if (idle) {
            prepareSongSheet();
        }
    }, [passages, currentPassage, selectedPassage, title, artist, idle]);

    const value = {
        modalType,
        passages,
        title,
        artist,
        selectedPassage,
        songsheet,
        setModalType,
        clearModal,
        updateTitle,
        updateArtist,
        selectPassage,
        addPassage,
        updatePassage,
        removePassage
    };

    return (<SongContext.Provider value={value}>{ children}</SongContext.Provider>)
}

export default SheetContext;