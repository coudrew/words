'use client'
import { useContext } from "react";
import { SongContext } from "./SheetContext";

function NotifyModal() {
    const { modalType, clearModal } = useContext(SongContext);
    const modalTextMap = {
        loading: 'loading...',
        error: 'Something went wrong'
    }
    return modalType ? (
        <div className="notifyModalContainer" data-testid="notifyModal">
            <div className="notifyModal">
                <p className="modalText" data-testid="notifyText">{modalTextMap[modalType]}</p>
                {modalType == 'error' ? <button onClick={clearModal} data-testid="modalOk" >Ok</button> : null}
            </div>
        </div>) : null
}

export default NotifyModal;