'use client'
import { useEffect, useRef, useContext } from 'react';
import Image from 'next/image';
import { SongContext } from './SheetContext';
import { createSongSheet, printToPDF } from '@/helpers/createSong';

function SheetViewer() {
    const { songsheet, title, artist, setModalType } = useContext(SongContext);
    const ref = useRef();
    useEffect(() => {
        if (ref.current) {
            const { song } = createSongSheet(songsheet, title, artist);
            ref.current.innerHTML = song;
        }
    }, [ref, songsheet, title, artist]);
    
    return <>
                <button className="button button--color-orange button--fixed button--square" onClick={() => printToPDF({id: 'song_sheet', title: 'Song Sheet'}, setModalType)} data-testid="print" >
                    <Image src={'/download.svg'} fill alt="print" />
                </button>
                <div id="song_sheet"><div className='cmTheme-dark1 viewer' ref={ref} /></div>
            </>
}

export default SheetViewer;