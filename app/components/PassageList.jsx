'use client'
import { useContext } from "react";
import { SongContext } from "./SheetContext";
import Image from "next/image";

export function PassageListItem({ passage, selected, selectPassage, testid }) {
    const { id, passageType } = passage;
    return (
        <button
            id={id}
            data-testid={testid}
            className={`button list-item${selected ? '--active' : ''}`}
            onClick={selectPassage}
        >
            {passageType}
        </button>)
};

function PassageList() {
    const { passages, selectPassage, selectedPassage = {}, addPassage, removePassage } = useContext(SongContext);
    const { id: selectedId } = selectedPassage;
    return (
        <div className="passageList">
            <div className="controls">
                <button className="button button--color-orange button--square" id={selectedId} onClick={() => addPassage(selectedPassage)} data-testid="copyPassage">
                    <Image src={'/content_copy.svg'} fill alt="copy" />
                </button>
                <button className="button button--color-orange button--square" onClick={() => addPassage()} data-testid="addPassage" >
                    <Image src={'/add.svg'} alt="add" fill />
                </button>
                <button className="button button--color-orange button--square" id={selectedId} onClick={removePassage} data-testid="removePassage" >
                    <Image src={'/remove.svg'} fill alt="remove"  />
                </button>
            </div>
            <div className="list">
                {passages.map((passage, i) =>
                    <PassageListItem
                        key={`passage-${i}`}
                        testid={`passage-${i}`}
                        passage={passage}
                        selectPassage={selectPassage}
                        selected={passage.id === selectedId}
                    />)}
            </div>
        </div>);
}

export default PassageList;