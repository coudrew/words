export const dynamic = 'force-dynamic';

export async function POST(request) {
    const html = await request.text();
    if (!html) {
        return new Response('html is required', { status: 400 });
    }
    const htmlBlob = new Blob([html], { type: 'text/html' });
    const formData = new FormData();
    try {
        const cssResponse = await fetch(process.env.SONGSHEET_CSS_URL);
        const cssBlob = await cssResponse.blob();
        formData.append('html', htmlBlob, 'index.html');
        formData.append('css', cssBlob, 'song_sheet.css');
    } catch (error) {
        return new Response('failed to fetch styling', { status: 501 });
    }
    try {
        const pdfResponse = await fetch(process.env.WEASYPRINT_SERVICE_URL, {
            method: 'POST',  
            body: formData
        });
        const pdfBlob = await pdfResponse.blob();
        return new Response(pdfBlob, {
            headers: {
                ...pdfResponse.headers,
                "content-disposition": `attachment; filename="unknown.pdf"`
            }
        });
    } catch (error) {
        return new Response('an internal server error occurred', { status: 500 });
    }
}