'use client'
import { useIsClient } from "@uidotdev/usehooks";
import SheetContext from "@/app/components/SheetContext";
import NotifyModal from "@/app/components/NotifyModal";
import App from "@/app/components/App";

export default function Home() {
  const isClient = useIsClient();
  return (
      <SheetContext>
      <NotifyModal />
      {isClient ? <App /> : null}
      </SheetContext>
  );
}
