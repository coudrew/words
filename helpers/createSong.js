import { parseSong, renderSong } from "chord-mark";

export function createSongSheet({song, chords}, title = 'Untitled', artist = '') {
    const preparedSong = parseSong(song);
    const renderedSong = renderSong(preparedSong, {autoRepeatChords: false});
    const titleTemplate = title ? `<div class="songTitle"><H2>${title}</H2><H3>${artist}</H3></div>` : '';
    return { song: titleTemplate + renderedSong, chords };
}

export async function printToPDF({ id, title }, setModal) {
    setModal('loading');
    const htmlString = `<html><head><title>${title}</title><link rel="stylesheet" href="/song_sheet.css"></head><body >${document.getElementById(id).innerHTML}</body></html>`;
    const response = await fetch('/api/pdf', { method: 'POST', body: htmlString });
    if (response.ok) {
        const blob = await response.blob();
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement('a');
        link.href = url;
        link.download = "songsheet.pdf";
        link.click();
        window.URL.revokeObjectURL(url);
        setModal('');
    } else {
        setModal('error');
    }
}