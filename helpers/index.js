import { uid } from "uid/single";

export function extractNonSubBeatChords(chordString) {
    return chordString.trim().split(' ').filter(Boolean);
};

export function capitalizeNoteName(chord) {
    return chord.replace(/(?<=(^|\s|\[|\/))[a-z]/g, match => match.toUpperCase());
};

export function extractChords(chordString) {
    const subBeatRegex = /\[[\w+\/\s]+\]/g;
    // remove beat duration markers
    const preparedChordString = chordString.replace(/[.]/g, '');
    // create array of sub-beat duration chords
    const subBeatChords = preparedChordString.match(subBeatRegex);
    let extractedChords;
    if (subBeatChords) {
        const nonSubBeatChords = preparedChordString.split(subBeatRegex);
        extractedChords = nonSubBeatChords.reduce((prev, curr) => (
            curr ? [...prev, ...extractNonSubBeatChords(curr)] : [...prev, subBeatChords.shift()]
        ), [])
    } else {
        extractedChords = extractNonSubBeatChords(preparedChordString);
    }
    return extractedChords;
};

export function preparePassage(passage) {
    const { lyrics = '', chords = '', passageType = 'verse' } = passage;
    // split lyircs and chords on newlines
    const lyricsArray = lyrics.split(/\n/g).map(line => line.trim());
    const chordsArray = chords.split(/\n/g).map(capitalizeNoteName);
    // create a set of only chords with no rhythm info
    const chordsOnly = chordsArray.reduce((acc, cur) => [
        ...acc, ...extractChords(cur)
    ], []).filter(chord => chord !== '%' && chord !== 'NC');
    const uniqueChordsOnly = new Set(chordsOnly);
    const typeMap = {
        verse: '#v',
        'pre-chorus': '#PreChorus',
        chorus: '#c',
        intro: '#i',
        bridge: '#b',
        solo: '#s',
        interlude: '#Interlude',
        adlib: '#Adlib',
        outro: '#Outro'
    };
    const passageString = `${typeMap[passageType] + '\n'}${chordsArray.map((chord, i) => `${chord}\n${lyricsArray[i] ? lyricsArray[i] + '\n' : ''}`).join('')}`;
    return { ...passage, passageString, uniqueChords: Array.from(uniqueChordsOnly) };
}

export function prepareSong(passages = []) {
    const songMap = passages.reduce((acc, cur) => ({
        song: acc.song + cur.passageString,
        chords: [...acc.chords, ...cur.uniqueChords.filter(chord => !acc.chords.includes(chord))]
    }), { song: '', chords: [], uniqueChords: [] });
    return songMap;
}

export class Passage {
    constructor(passage = {}) {
        const { lyrics = '', chords = '', passageType = 'verse', passageString = '', uniqueChords = [] } = passage;
        this.lyrics = lyrics;
        this.chords = chords;
        this.passageString = passageString;
        this.passageType = passageType;
        this.uniqueChords = uniqueChords;
        this.id = uid();
        this.cursor = {};
    }
}